from grid_2048 import *
from textual_2048 import *

def random_play():
    #on suppose que le jeu par defaut comprend une grille de 4x4

    #phase d'initialisation du jeu
    grid_game=init_game(4)
    print(grid_to_string_with_size_and_theme(grid_game,THEMES["0"],4))

    #phase de jeu
    while not is_game_over(grid_game):

        #pour chaque tour
        d=rd.choice(list_move_possible(grid_game)) #on choisi un mouvement
        grid_game=move_grid(grid_game,d) #on le fait
        grid_game=grid_add_new_tile(grid_game) #une tuile se crée
        print(grid_to_string_with_size_and_theme(grid_game,THEMES["0"],4)) #on affuche le résulat
        print("\n***********************************\n") #permet aux différentes grilles d'être plus lisible

    #vérification si le jeu est gagnant ou perdant

    if did_you_win(grid_game):
        print("CONGRATS ! YOU WON !!!")
    else :
        print ("YOU FAIL, TRY AGAIN")
    return

def game_play():

    # Saisie des paramètres
    size = read_size_grid()
    theme = read_theme_grid()

    # Initialisation
    grid = init_game(size)
    print(grid_to_string_with_size_and_theme(grid,theme,size))

    # Phase de jeu
    while not is_game_over(grid):
        direction = read_player_command() # Choix de la direction
        possibles = list_move_possible(grid)
        if direction in possibles:
            grid = move_grid(grid,direction)
            grid = grid_add_new_tile(grid)
        print(grid_to_string_with_size_and_theme(grid,theme,size))
        print("\n***********************************\n")

    # Test de victoire
    if did_you_win(grid_game):
        print("CONGRATS ! YOU WON !!!")
    else :
        print ("YOU FAIL, TRY AGAIN")
    return
