THEMES = {"0": {"name": "Default", 0: "", 2: "2", 4: "4", 8: "8", 16: "16", 32:"32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "2048",4096: "4096", 8192: "8192"}, "1": {"name": "Chemistry", 0: "", 2: "H", 4: "He", 8:"Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048:"Na", 4096: "Mg", 8192: "Al"}, "2": {"name": "Alphabet", 0: "", 2: "A", 4: "B", 8:"C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K",4096: "L", 8192: "M"}}

# FONCTIONNALITE 3

def read_player_command():
    deplacements = {'g':"left",'d':"right",'h':"up",'b':"down"}
    move = input("Entrez votre commande (g (gauche), d (droite), h (haut), b(bas)): ")
    while move not in ['g','d','h','b']:
        move = input("Commande invalide. \n Entrez votre commande (g (gauche), d (droite), h (haut), b(bas)): ")
    return deplacements[move]

def read_size_grid():
    size = input("Choisissez la taille de la grille : ")
    while True:
        try:
            size = int(size)
        except:
            size = input("Taille invalide, choisissez un nombre entier. \n Choisissez la taille de la grille : ")
        else:
            break
    return size

def read_theme_grid():
    theme = input("Choisissez le numéro du thème : ")
    while theme not in ['0','1','2']:
        theme = input("Ce thème n'existe pas ! \n Choisissez le numéro du thème : ")
    return THEMES[theme]
