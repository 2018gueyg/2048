from tkinter import *
from tkinter.messagebox import *
from run_2048 import *
from functools import partial

THEMES = {"0": {"name": "Default", 0: "", 2: "2", 4: "4", 8: "8", 16: "16", 32:"32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "2048",4096: "4096", 8192: "8192"}, "1": {"name": "Chemistry", 0: "", 2: "H", 4: "He", 8:"Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048:"Na", 4096: "Mg", 8192: "Al"}, "2": {"name": "Alphabet", 0: "", 2: "A", 4: "B", 8:"C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K",4096: "L", 8192: "M"}}

TILES_BG_COLOR = {0: "#9e948a", 2: "#eee4da", 4: "#ede0c8", 8: "#f1b078", \
                  16: "#eb8c52", 32: "#f67c5f", 64: "#f65e3b", \
                  128: "#edcf72", 256: "#edcc61", 512: "#edc850", \
                  1024: "#edc53f", 2048: "#edc22e", 4096: "#5eda92", \
                  8192: "#24ba63"}

TILES_FG_COLOR = {0: "#776e65", 2: "#776e65", 4: "#776e65", 8: "#f9f6f2", \
                  16: "#f9f6f2", 32: "#f9f6f2", 64: "#f9f6f2", 128: "#f9f6f2", \
                  256: "#f9f6f2", 512: "#f9f6f2", 1024: "#f9f6f2", \
                  2048: "#f9f6f2", 4096: "#f9f6f2", 8192: "#f9f6f2"}

TILES_FONT = {"Verdana", 40, "bold"}

def graphical_update():
    """Fonction qui met à jour les labels avec les valeurs de game_grid."""
    global graphical_grid,game_grid,grid_size,theme
    for i in range(grid_size):
        for j in range(grid_size):
            graphical_grid[i][j].config(bg = TILES_BG_COLOR[game_grid[i][j]], fg = TILES_FG_COLOR[game_grid[i][j]], text = theme[game_grid[i][j]])
    return

def grid_update(direction,fen):
    """Fonction lancée quand un coup est joué. Vérifie que le jeu n'est pas fini et qu'il est valable avant de l'exécuter.
    Gère aussi la fin du jeu."""
    global graphical_grid,game_grid,grid_size,theme,score
    if not is_game_over(game_grid):
        possibles = list_move_possible(game_grid)
        if direction in possibles:
            game_grid = move_grid(game_grid,direction)
            game_grid = grid_add_new_tile(game_grid)
            score.set(str(sum(get_all_tiles(game_grid))))
        graphical_update()
    elif did_you_win(game_grid):
        showinfo("Victoire","Vous avez gagné !")
    else:
        showinfo("Echec","Vous avez perdu...")


root = Tk()
background = Frame(root)
graphical_grid = []
labels = []
game_grid = []

def launch_game():
    # Lance le jeu : initalise la grille et crée les labels.
    global graphical_grid,game_grid,grid_size,theme

    grid_size = int(choix_size.get())
    theme = THEMES[str(choix_theme.curselection()[0])]

    game_grid = init_game(grid_size)

    for i in range(grid_size):
        graphical_grid.append([])
        labels.append([])
        for j in range(grid_size):
            graphical_grid[i].append(Label(master = background, bd = 1,relief = "ridge", bg = TILES_BG_COLOR[0], text = "",font = TILES_FONT,height = 5, width = 10))
            graphical_grid[i][j].grid(column = j, row = i)
    graphical_update()

background.pack()

root.bind("<KeyPress-Down>",partial(grid_update,"down"))
root.bind("<KeyPress-Up>",partial(grid_update,"up"))
root.bind("<KeyPress-Right>",partial(grid_update,"right"))
root.bind("<KeyPress-Left>",partial(grid_update,"left"))

param = Tk()

grid_size = 4
theme = THEMES['0']

label_size = Label(param,text = "Choisir la taille de la grille :")
label_size.pack()

choix_size = Entry(param)
choix_size.insert(END,"4")
choix_size.pack()

label_theme = Label(param,text = "Choisir un thème :")
label_theme.pack()

choix_theme = Listbox(param,height = 4,selectmode = "single")
choix_theme.insert(0,"Default")
choix_theme.insert(1,"Chemistry")
choix_theme.insert(2,"Alphabet")
choix_theme.select_set(0)
choix_theme.pack()

label_score = Label(param,text = "Score =")
label_score.pack()
score = StringVar()
score.set("0")
affiche_score = Label(param,textvariable = score)
affiche_score.pack()


play = Button(param,text = "Play",command = launch_game)
play.pack(side = RIGHT)

quit = Button(param,text = "Exit",command = exit)
quit.pack(side = LEFT)

root.mainloop()
