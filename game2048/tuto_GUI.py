from tkinter import *

"""window=Tk()
label_field = Label(window,text="Hello World !")
label_field.pack(fill=BOTH)
window.mainloop()"""

#disposition :
# dans .pack
#placement cardinal : side =(Tkinter.TOP, Tkinter.LEFT, Tkinter.RIGHT, Tkinter.BOTTOM
# grosseur de la fenetre : fill= X,Y,BOTH

#acction
#les boutons :
#Les boutons (Button) sont des widgets sur lesquels on peut cliquer et qui peuvent déclencher des actions ou commandes.

"""def write_text():
    print("Hello CentraleSupelec")

root = Tk()
frame =Frame(root)
frame.pack()

button = Button(frame,
                   text="QUIT",
                   activebackground = "blue",
                   fg="red",
                   command=quit)
button.pack(side=LEFT)
slogan = Button(frame,
                   fg="blue",
                   text="Hello",
                   command=write_text)
slogan.pack(side=LEFT)

root.mainloop()"""

"""from tkinter import Tk, StringVar, Label, Entry, Button
from functools import partial

def update_label(label, stringvar):
    
    #Met à jour le texte d'un label en utilisant une StringVar.
    
    text = stringvar.get()
    label.config(text=text)
    stringvar.set('merci')

root = Tk()
text = StringVar(root)
label = Label(root, text='Your name')
entry_name = Entry(root, textvariable=text)
button = Button(root, text='clic',
                command=partial(update_label, label, text))

label.grid(column=0, row=0)
entry_name.grid(column=0, row=1)
button.grid(column=0, row=2)

root.mainloop()"""


"""from tkinter import Tk, Label, Frame

root = Tk()
f1 = Frame(root, bd=1, relief='solid')
Label(f1, text='je suis dans F1').grid(row=0, column=0)
Label(f1, text='moi aussi dans F1').grid(row=0, column=1)

f1.grid(row=0, column=0)
Label(root, text='je suis dans root').grid(row=1, column=0)
Label(root, text='moi aussi dans root').grid(row=2, column=0)

root.mainloop()"""
