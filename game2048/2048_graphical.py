from tkinter import *
from game2048.grid_2048 import *
from functools import partial

# DICTIONNAIRE
TILES_BG_COLOR = {0: "pink", 2: "magenta", 4: "purple", 8: "blue", \
                  16: "#eb8c52", 32: "#f67c5f", 64: "#f65e3b", \
                  128: "#edcf72", 256: "#edcc61", 512: "#edc850", \
                  1024: "#edc53f", 2048: "#edc22e", 4096: "#5eda92", \
                  8192: "#24ba63"}

TILES_FG_COLOR = {0: "pink", 2: "black", 4: "white", 8: "#f9f6f2", \
                  16: "#f9f6f2", 32: "#f9f6f2", 64: "#f9f6f2", 128: "#f9f6f2", \
                  256: "#f9f6f2", 512: "#f9f6f2", 1024: "#f9f6f2", \
                  2048: "#f9f6f2", 4096: "#f9f6f2", 8192: "#f9f6f2"}

TILES_FONT = {"Verdana", 80, "bold"}
#FIN DICTIONNAIRE

#FONCTIONS

def graphical_inti():
    #ouvertire du widget principal
    game_2048=Tk()
    fenetre=Toplevel()
    fenetre.title("2048")
    game_2048.title("2048")
    return(game_2048)

#attention il faudra toujours faire game_2048.mainloop() à la fin du programme


def create_empty_grid():
    list_tuile=[]
    for ligne in range (4):
        list_tuile.append([])
        for colonne in range (4):
            #création des tuiles
            list_tuile[ligne].append(Frame(game_2048,borderwidth=2, relief="solid",height = 100, width = 100,bg=TILES_BG_COLOR[0]))
            #positionnement des tuiles
            list_tuile[ligne][colonne].grid(row=ligne,column=colonne)
    return(list_tuile)

#ATTENTION lors de la création d'un frame, sa hauteur et sa largeur vaut 0 de base : modifier directement lors de la création par height= et width=


def update_graphical_grid():
    global grid_game
    """première fonction de maj de l'affichage
    idée de ce programme : on va crée des tuiles pour les former la grille, et ajouter des labels correspondant au 
    numéro des cases pour les cases non vide"""
    #cette fonction ne marche que pour les grilles 4x4
    #création de tuile
    list_tuile=[]
    #création des labels
    list_label=[]
    for ligne in range (4):
        list_tuile.append([])
        list_label.append([])

        #pour chaque ligne
        for colonne in range (4):
            #on liste les colonnes où il va y avoir des numéros
            list_colonne_wt_label=[]
            #on regarde la valeur de la tuile
            value_tuile=grid_game[ligne][colonne]
            #si la valeur est zero, il n'y a pas de label
            if value_tuile==0 or value_tuile==' ':
                list_tuile[ligne].append(Frame(game_2048,borderwidth=1, relief="solid",height = 100, width = 100,bg=TILES_BG_COLOR[0]))
            #sinon on dit crée un label et retenir sa position pour le positionner ensuite
            else :
                #création de la tuile
                list_tuile[ligne].append(Frame(game_2048,borderwidth=2,height = 100, width = 100))
                    #création du label
                list_label[ligne].append(Label(list_tuile[ligne][colonne],text=str(value_tuile),fg=TILES_FG_COLOR[value_tuile],bg=TILES_BG_COLOR[value_tuile],height = 3, width = 7,font=TILES_FONT,bd={2,"black"}))
                    #mémoire de sa position
                list_colonne_wt_label.append(colonne)
            #on positionne correctement le label
            for lab in range (len(list_colonne_wt_label)):
                list_label[ligne][lab].grid(row=ligne,column=list_colonne_wt_label[lab])
            #on positionne correctement les labels et les tuiles
            list_label[ligne][colonne].grid(row=ligne,column=colonne)
            list_tuile[ligne][colonne].grid(row=ligne,column=colonne)
    return(list_tuile)

"""problème de cette fonction : 
        affichage : certains label n'apparaissent pas 
                    il y a concurence entre les labels et les tuiles ce qui est peu esthétique
        temps d'éxécution : la maj entre chaque action du joueur est très longue (5 à 10 sec)"""

def update_graphical_grid_fix():
    global grid_game, grid_size,score_compteur
    #cette fonction ne crée que des labels mais est plus rapide et ne change pas l'esthétisme
    #on upgrade l'affichage pour afficher un score
    #affichage score
    score=Label(game_2048,text="Score : "+str(score_compteur),height=0,width=0)
    score.grid(row=grid_size,column=1,columnspan=grid_size+1)

    #affichage des tuiles
    list_label=[]
    for ligne in range (grid_size):
        list_label.append([])

        #pour chaque ligne
        for colonne in range (grid_size):
            #on regarde la valeur de la tuile
            value_tuile=grid_game[ligne][colonne]
            #création du label
            list_label[ligne].append(Label(game_2048,text=str(value_tuile),fg=TILES_FG_COLOR[value_tuile],bg=TILES_BG_COLOR[value_tuile],height = 3, width = 7,font=TILES_FONT,bd=1,relief="solid"))
            #on positionne correctement le label
            list_label[ligne][colonne].grid(row=ligne,column=colonne)
            """pour palier au problème des tuiles vides, j'écris le "0" de la même coouleur que le fond"""
    return(list_label)

#definition d'un minuteur (finalement ne sert pas)
def timer(left):
    global debut_jeu
    if left > 0:
        debut_jeu.after(200, lambda z=left-1: timer(z))
    else:
        debut_jeu.quit()


def update_grid_mov (direction,fen):
    #fonction liant action clavier et action sur le jeu
    global grid_game,oups,score_compteur
    #on regarde si le jeu est fini ou non
    if not is_game_over(grid_game):
        #on regarde si la direction indiquée est valide
        possibles = list_move_possible(grid_game)
        if direction in possibles:
            if direction=="left":
                    grid_game=move_grid(grid_game,"left")
            elif direction=="right":
                        grid_game=move_grid(grid_game,"right")
            elif direction=="down":
                    grid_game=move_grid(grid_game,"down")
            else:
                    grid_game=move_grid(grid_game,"up")
            grid_game,new_value=grid_add_new_tile_graphical(grid_game)
            score_compteur=score_compteur+new_value
            update_graphical_grid_fix()
        else :
            #si elle ne l'est pas : message d'erreur
            oups=Tk()
            oups.title("Oups !")
            label = Label(oups, text='Ce mouvement est impossible                           ')
            label.grid(column=0, row=0)
            oups.mainloop()
            exit()

    #affichage de la fenetre de victoire/défaite
    elif did_you_win(grid_game):
        felic=Tk()
        felic.title("FELICITATION!")
        label = Label(felic, text='Vous avez gagné !           ')
        label.grid(column=0, row=0)
        debut_jeu=Button(felic,text="Rejouer",command=relance_jeu)
        #la commande precedante permet de rejouer mais est peu performante !
        debut_jeu.grid(column=1, row=1)
        felic.mainloop()
    else:
        domma=Tk()
        domma.title("DOMMAGE!")
        label = Label(domma, text='Perdu ! Essayez encore ;)           ')
        label.grid(column=0, row=0)
        debut_jeu=Button(domma,text="Rejouer",command=relance_jeu)
           #la commande precedante permet de rejouer mais est peu performante !
        debut_jeu.grid(column=1, row=1)
        domma.mainloop()
    return()

def relance_jeu ():
    global score_compteur,relance,taille_choisie
    #réinitialise le compteur du score
    score_compteur=0
    relance = Tk()
    relance.title("Choisir les paramètres du jeu")
    label = Label(relance, text='Entrer la taille de jeu que vous souhaitez            ')
    taille_choisie = Entry(relance)
    label.grid(column=0, row=0)
    debut_jeu_bis=Button(relance,text="C'est parti",command=lancement_debut_jeu_bis)
    debut_jeu_bis.grid(column=1,row=1)
    taille_choisie.grid(column=0, row=1)



def lancement_debut_jeu():
    global grid_game,graphical_grid,grid_size,score_compteur
    #releve la taille du jeu choisi par l'utilisateur
    grid_size=int(taille_choisie.get())
    #initialise le jeu
    grid_game,score_compteur=init_game_graphical(grid_size)
    background=Frame(game_2048)
    update_graphical_grid_fix()
    #ferme la fenetre de choix de taille
    choix_taille.destroy()

def lancement_debut_jeu_bis():
    global grid_game,graphical_grid,grid_size,score_compteur
    grid_size=int(taille_choisie.get())
    grid_game,score_compteur=init_game_graphical(grid_size)
    background=Frame(game_2048)
    update_graphical_grid_fix()
    relance.destroy()

#FIN FONCTION


#EXECUTION

    #fenetre pour choisir la taille de la grille et lancer le jeu
choix_taille = Tk()
choix_taille.title("Choisir les paramètres du jeu")
        #instructions
label = Label(choix_taille, text='Entrer la taille de jeu que vous souhaitez : ')
label2 = Label(choix_taille, text='              ')
instruction = Label(choix_taille, text='Hint 1 : appuyer sur "Commencer à jouer" pour lancer le jeu',fg="purple")
instruction2=Label(choix_taille, text='Hint 2 : utilisez les fleches pour déplacer les cases',fg="purple")
instruction.grid(column=0,row=3)
instruction2.grid(column=0,row=4)
label2.grid(column=1,row=1)
label.grid(column=0, row=0)
        #choix de la taille
taille_choisie = Entry(choix_taille)
taille_choisie.grid(column=0, row=1)

    #on crée un score : la somme des valeurs des tuiles qui sont apparuesb depuis le début du jeu
score_compteur=0

    #lancment de début de jeu
debut_jeu=Button(choix_taille,text="Commencer à jouer",command=lancement_debut_jeu,fg="blue")
debut_jeu.grid(column=3, row=1)
game_2048=graphical_inti()
        #on initialise la grille
grid_size=0
grid_game=[]

    #on défini les actions possibles pour jouer au jeu
game_2048.bind("<KeyPress-Down>",partial(update_grid_mov,"down"))
game_2048.bind("<KeyPress-Up>",partial(update_grid_mov,"up"))
game_2048.bind("<KeyPress-Right>",partial(update_grid_mov,"right"))
game_2048.bind("<KeyPress-Left>",partial(update_grid_mov,"left"))
    #apres chaque modification on doit modifier l'affichage
update_graphical_grid_fix()

    #on clot toute les TK() qu'on a ouverte
choix_taille.mainloop()
game_2048.mainloop()
